## How to set a restartable forwarding server

## enable forwarding 
```
echo 1 > /proc/sys/net/ipv4/ip_forward
iptables -t nat -A PREROUTING -p tcp --dport 443 -j DNAT --to-destination <addtional_ip>:8989
iptables -t nat -A POSTROUTING -p tcp -d <addtional_ip> --dport 8989 -j SNAT --to-source <main_ip>
```

## add them to `/etc/rc.local`