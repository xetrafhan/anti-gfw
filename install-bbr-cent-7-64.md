* essential features:
    - (Ipv6 support) - VPS 1
    - (CentOS 7 x64) - VPS 1

 

# Install google bbr
```
sudo rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
sudo rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-2.el7.elrepo.noarch.rpm
```
```
sudo yum --enablerepo=elrepo-kernel install kernel-ml -y
```
```
rpm -qa | grep kernel
```
output
```
kernel-ml-4.9.0-1.el7.elrepo.x86_64
kernel-3.10.0-514.el7.x86_64
kernel-tools-libs-3.10.0-514.2.2.el7.x86_64
kernel-tools-3.10.0-514.2.2.el7.x86_64
kernel-3.10.0-514.2.2.el7.x86_64
```
```
sudo egrep ^menuentry /etc/grub2.cfg | cut -f 2 -d \'
```
output
```
CentOS Linux 7 Rescue a0cbf86a6ef1416a8812657bb4f2b860 (4.9.0-1.el7.elrepo.x86_64)
CentOS Linux (4.9.0-1.el7.elrepo.x86_64) 7 (Core)
CentOS Linux (3.10.0-514.2.2.el7.x86_64) 7 (Core)
CentOS Linux (3.10.0-514.el7.x86_64) 7 (Core)
CentOS Linux (0-rescue-bf94f46c6bd04792a6a42c91bae645f7) 7 (Core)
```
Since the line count starts at 0 and the 4.9.0 kernel entry is on the second line, set the default boot entry as 1:
```
sudo grub2-set-default 1
```
```
sudo shutdown -r now
```
```
uname -r
```
output
```
4.9.0-1.el7.elrepo.x86_64
```
```
echo 'net.core.default_qdisc=fq' | sudo tee -a /etc/sysctl.conf
echo 'net.ipv4.tcp_congestion_control=bbr' | sudo tee -a /etc/sysctl.conf
sudo sysctl -p
```
```
sudo sysctl net.ipv4.tcp_available_congestion_control
```
output
```
net.ipv4.tcp_available_congestion_control = bbr cubic reno
```
```
sudo sysctl -n net.ipv4.tcp_congestion_control
```
output
```
bbr
```
```
lsmod | grep bbr
```
output:
```
tcp_bbr                16384  0
```