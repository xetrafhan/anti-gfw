# first, install original bbr
```
apt-get update && wget --no-check-certificate -qO 'bbr-on-ubuntu-16.sh' 'https://bitbucket.org/xetrafhan/anti-gfw/raw/3ebcd6b971100bdb0967201e8da067df7f7e171d/install-bbr-on-ubuntu-16.sh' && chmod a+x bbr-on-ubuntu-16.sh && bash bbr-on-ubuntu-16.sh -f
```

# install powered version bbr
```bash
apt-get update && wget --no-check-certificate -qO 'BBR_POWERED.sh' 'https://bitbucket.org/xetrafhan/anti-gfw/raw/cfeddf83ed1b3f105cc8fd1d390acf9cc11efb9f/BBR_POWERED.sh' && chmod a+x BBR_POWERED.sh && bash BBR_POWERED.sh -f
```

本脚本在Debian8,Debian9,Ubuntu16.04上通过测试.

