# the process of setting up and shadowsocks-r TCP relay anti-gfw service

# start 2 server in:

| name | foll | 
|------|------|
| PROTECTED: SSR VPS | VPS 1 |
| PROTECTED: ROUTER VPS | VPS 2 |

* essential features:
    - (Ipv6 support) - VPS 1
    - (CentOS 6 x64) - VPS 1
    - (Ubuntu 16.04) - VPS 2

# test vps 2's connectivity
```
ping VPSIP2
```

# create a ssr server config with 

| key | value |
|-----|-------|
| ip | VPSIP 2 |
| pass | wpoipgepa | 
| encry | aes-128-cfb | 
| obsf | auth_aes128_sha1 | 
| obsf2 | http-simple |
| port | 443 | 

# set ip environment variables:
```
export VPSIP1=<replace your ip of jp vps 1>
export VPSIP2=<replace your ip of jp vps 2>
```

# replace it with your password:
* vps1pass: 
* vps2pass:

# change kernel of VPS 1:
```
wget https://bitbucket.org/xetrafhan/anti-gfw/raw/b95df58520e5c5579c780a1633b3537a362383a9/kernel-2.6.32-504.3.3.el6.x86_64.rpm
rpm -ivh kernel-2.6.32-504.3.3.el6.x86_64.rpm --force
reboot
```

# VPS2 (iptable forwading and server speeder installation):
```
echo 1 > /proc/sys/net/ipv4/ip_forward

iptables -t nat -A PREROUTING -p tcp --dport 443 -j DNAT --to-destination <US_VPS_IP>:8989
iptables -t nat -A POSTROUTING -p tcp -d <US_VPS_IP> --dport 8989 -j SNAT --to-source <JAPAN_VPS_IP>
```
nano /etc/rc.local
```
iptables -t nat -A PREROUTING -p tcp --dport 443 -j DNAT --to-destination <US_VPS_IP>:8989
iptables -t nat -A POSTROUTING -p tcp -d <US_VPS_IP> --dport 8989 -j SNAT --to-source <JAPAN_VPS_IP>
```

# VPS2 (firewall configuration):
* ACCEPT: `443` with `TCP`
* ACCEPT: `8989` with `TCP` from `US_VPS_IP/32`

# VPS1 (install shadowsocks-r):
Install serverspeeder first:
```
wget --no-check-certificate -O appex.sh https://raw.githubusercontent.com/0oVicero0/serverSpeeser_Install/master/appex.sh && chmod +x appex.sh && bash appex.sh install
```
```
/appex/bin/serverSpeeder.sh restart
```
install shadowsocks-r
```
yum update -y && yum install -y wget net-tools vim && wget https://bitbucket.org/xetrafhan/anti-gfw/raw/b95df58520e5c5579c780a1633b3537a362383a9/shadowsocks-all.sh && bash ./shadowsocks-all.sh
```

| key | value | 
|----|------|
| key | wpoipgepa | 
| encry | aes-128-cfb | 
| obsf | auth_aes128_sha1 | 
| obsf2 | http-simple | 
| port | 8989 | 

instsall openvpn
```
wget https://bitbucket.org/xetrafhan/anti-gfw/raw/b95df58520e5c5579c780a1633b3537a362383a9/openvpn-install.sh
chmod +x openvpn-install.sh
./openvpn-install.sh
```

| key | value |
|-----|------|
| name | hfclient |
| proto | TCP | 
| protect | moderate |
| port | 8990 |

```
./openvpn-install.sh
```
| key | value |
|-----|------|
| name | bzpclient |
| proto | TCP | 
| protect | moderate |
| port | 8990 |

auto-start script
```
vi /etc/rc.local 
```
```
iptables-restore -c < /etc/iptables/iptables.rules
echo 3 > /proc/sys/net/ipv4/tcp_fastopen
/etc/init.d/shadowsocks-r restart
service openvpn restart
chkconfig openvpn on
/appex/bin/serverSpeeder.sh restart
```

append to tcp fast open
```
vi /etc/sysctl.conf
```
```
net.ipv4.tcp_fastopen = 3
```

change config.json
```
vi /etc/shadowsocks-r/config.json
```
```
change tcp_fast_open to true, change dns_ipv6 to true
```

restart ssr
```
/etc/init.d/shadowsocks-r restart
```

# copy openvpn configuration back
```
scp root@<VPS1>:/root/hfclient.ovpn <>
scp root@<VPS1>:/root/bzpclient.ovpn <>
```

# set vps 1's firewall(name: `ssrovpn-firewall`)
* only accept `8989` and `8990` from anywhere with TCP protocol