# install shadowsocks-r
```
yum update -y && yum install -y wget net-tools vim && wget https://bitbucket.org/xetrafhan/anti-gfw/raw/b95df58520e5c5579c780a1633b3537a362383a9/shadowsocks-all.sh && bash ./shadowsocks-all.sh
```
 
| key | value | 
|----|------|
| key | wpoipgepa | 
| encry | aes-256-cfb | 
| obsf | auth_aes128_sha1 | 
| obsf2 | tls1.2_ticket_auth | 
| port | 8989 | 

instsall openvpn
```
wget https://bitbucket.org/xetrafhan/anti-gfw/raw/b95df58520e5c5579c780a1633b3537a362383a9/openvpn-install.sh
chmod +x openvpn-install.sh
./openvpn-install.sh
```

| key | value |
|-----|------|
| name | hfclient |
| proto | TCP | 
| protect | moderate |
| port | 8990 |

```
./openvpn-install.sh
```
| key | value |
|-----|------|
| name | bzpclient |
| proto | TCP | 
| protect | moderate |
| port | 8990 |

# add an addtional ip to server and reboot then set up the ip and restart network service
    - Dual IP (`main_ip` and `addtional_ip`)
    - `service network restart`
    - set ip reverse dns to `www.qiniu.com`
    
auto-start script
```
vi /etc/rc.local 
```
```
iptables-restore -c < /etc/iptables/iptables.rules
echo 1 > /proc/sys/net/ipv4/ip_forward
iptables -t nat -A PREROUTING -p tcp --dport 443 -j DNAT --to-destination <addtional_ip>:8989
iptables -t nat -A POSTROUTING -p tcp -d <addtional_ip> --dport 8989 -j SNAT --to-source <main_ip>
echo 3 > /proc/sys/net/ipv4/tcp_fastopen
/etc/init.d/shadowsocks-r restart
service openvpn restart
chkconfig openvpn on
```

append to tcp fast open
```
vi /etc/sysctl.conf
```
```
net.ipv4.tcp_fastopen = 3
```

change config.json
```
vi /etc/shadowsocks-r/config.json
```
```
change tcp_fast_open to true, change dns_ipv6 to true, change ip to `main_ip`
```

restart ssr and reboot
```
/etc/init.d/shadowsocks-r restart
reboot
```

# copy openvpn configuration back
```
scp root@<main_ip>:/root/<digital_ocean>hfclient.ovpn <>
scp root@<main_ip>:/root/<digital_ocean>bzpclient.ovpn <>
```

# firewall configuration
* `PROTECTED: ANTI_GFW_FIREWALL`
* ACCEPT: `443` with `TCP`
* ACCEPT: `8989` and `8990` with `TCP` from `addtional_ip/32`

# create a ssr server config with 

| key | value |
|-----|-------|
| ip | `addtional_ip` |
| pass | wpoipgepa | 
| encry | aes-128-cfb | 
| obsf | auth_aes128_sha1 | 
| obsf2 | http-simple |
| port | 443 | 